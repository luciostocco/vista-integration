<?php
/**
 * Created by PhpStorm.
 * User: lucio
 * Date: 10/09/17
 * Time: 02:27
 */

// Register the Autoload
spl_autoload_register(function ($class_name) {
    // Exclude the namespace from the $class_name
    $class_name = substr(substr($class_name, strpos($class_name, '\\')), 1);
    $class_name = 'src/' . $class_name;
    if (file_exists($class_name . '.php')) {
        include $class_name . '.php';
    } else {
        echo 'Unable to load ' . ' Class '. $class_name . '.php';
        die();
    }
});
