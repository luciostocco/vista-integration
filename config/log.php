<?php
return [
    "spacer" => "+-- ",
    "header" => [
        "color"  => "brown",
        "format" => "[^PID, %H:%i:%s]"
    ],
    "name" => "queue"
];