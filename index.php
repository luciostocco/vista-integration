<?php

/**
 * Created by PhpStorm.
 * User: lucio
 * Date: 01/09/17
 * Time: 17:40
 */

namespace VistaAPIClient;

use VistaIntegration\LoadConfig;
use VistaIntegration\LogConsole;
use VistaIntegration\VistaApiClient;
use VistaIntegration\WpApiClient;
use VistaIntegration\StoreReferenceToExclude;
use VistaIntegration\DeleteWpStatic;

require "vendor/autoload.php"; // Autoload composer
require "autoload.php";        // Autoload App

// Send all errors to log file
ini_set("log_errors", 1);
ini_set("error_log", dirname(__FILE__, 2)."/log/queue.log");

LogConsole::configure();

$vista_api = new VistaApiClient(LoadConfig::VISTA_ACCESS_TOKEN);
$wp_api    = new WpApiClient();

//log::warning(" ");
$dateTimeScriptStarted = date('Y-m-d H:i');
$timeStarted   = date('H:i:s');

LogConsole::printMessage("warning", [
    " ",
    " ",
    "Script starting....",
    "Started: ".$dateTimeScriptStarted
]);

// 1) Load all cinemas from Vista
$vista_cinemas = $vista_api->loadCinemas();

// Load data from wordpress
$wp_sessions   = $wp_api->loadSessions();  // Load all sessions from wordpress
$wp_cinemas    = $wp_api->loadCinemas();   // Load cinemas from wordpress
$wp_films      = $wp_api->loadFilms();     // Load Films from wordpress

$vista_film_genres = $vista_api->loadFilmGenres();


/* ********************
 *  Main Workflow
 **********************/

$cinema_id_created = " ";

$ActiveRegister = new StoreReferenceToExclude();

// 1) list of all Cinemas
foreach ($vista_cinemas->value as $cinema) {

    $ActiveRegister->Vista_TotalActivesCinema[] = $cinema->ID;

    // 2) Create or updating cinema in Wordpress
    $wp_api->cinemaOperations($wp_cinemas, $cinema, $cinema_id_created);

    $cinema_search = urlencode("'$cinema->ID'");

    // 3) Requesting a list of films that are currently showing at a particular Cinema
    $vista_films_cinema = $vista_api->loadFilmsCurrentlyShowingByCinema($cinema->ID);

        /*
        *  NAO ESQUECER DE TIRAR A TRAVA...
        *
        */

    $k=0;

    foreach ($vista_films_cinema->value as $film ) {

        /*
         *  NAO ESQUECER DE TIRAR A TRAVA...
         *
         */
  //        if ($k==3) { break; } else ++$k;

        $ActiveRegister->Vista_TotalActivesFilms[] = $film->FilmHOPK;

        // Loading Sessions currently showing in a particular Cinema and Film
        $sessions_film_cinema = $vista_api->loadSessionsShowingByFilmAndCinema($cinema->ID, $film->FilmHOPK);

        $session_array = array();


        // 6) REQUESTING EXTENDED FILM DETAILS
        $extended_film_details = $vista_api->requestExtendedFilmDetails($film->FilmHOPK);

        $extended_film_details['director'] = "";    // TIRAR
        $extended_film_details['cast'] = "";        // TIRAR


        $film_id_created = 0;  // store last film id created

        // 7) Creating or Updating the film and its relationship between Sessions in Wordpress
        $wp_api->filmOperations($wp_films, $film, $session_array, $extended_film_details, $cinema->ID, $cinema_id_created, $vista_film_genres, $vista_api, $film_id_created);


        /*
         *  NAO ESQUECER DE TIRAR A TRAVA...
         *
         */
        $i=0;


        // 4) Create/update Sessions for a particular film and cinema
         foreach ($sessions_film_cinema->value as $session ) {
             /*
              *  NAO ESQUECER DE TIRAR A TRAVA...
              *
              */
  //           if ($i==5) { break; } else ++$i;

             $ActiveRegister->Vista_TotalActivesSessions[] = $session->SessionId;

             // 5) Create or updating sessions in Wordpress
             // $session_array is passed by reference
             $wp_api->sessionsOperations($wp_sessions, $session, $cinema, $film, $session_array, $cinema_id_created, $film_id_created);
        }

        // 6) REQUESTING EXTENDED FILM DETAILS
    //    $extended_film_details = $vista_api->requestExtendedFilmDetails($film->FilmHOPK);

    //    $extended_film_details['director'] = "";    // TIRAR
    //    $extended_film_details['cast'] = "";        // TIRAR

        // 7) Creating or Updating the film and its relationship between Sessions in Wordpress
    //    $wp_api->filmOperations($wp_films, $film, $session_array, $extended_film_details, $cinema->ID, $vista_film_genres, $vista_api);

    }
}

// Exclude no used registers in Wordpress
DeleteWpStatic::ExcludeSessions($wp_sessions, $ActiveRegister, $wp_api);
DeleteWpStatic::ExcludeFilms($wp_films, $ActiveRegister, $wp_api);
DeleteWpStatic::ExcludeCinemas($wp_cinemas , $ActiveRegister, $wp_api);


$dateFinalized = date('H:i:s');
$timefinalized = date('H:i:s');

LogConsole::printMessage("warning", [
    " ",
    "Script successfully completed....",
    "Started: " . $dateTimeScriptStarted." | Finalized: ".$dateFinalized ,
    "Runtime: " . date('H:i:s',(strtotime($timefinalized) - strtotime($timeStarted)))
]);






