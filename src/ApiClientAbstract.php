<?php
/**
 * Created by PhpStorm.
 * User: lucio
 * Date: 09/09/17
 * Time: 20:31
 */

namespace VistaIntegration;

/******************************************
 * Class ApiClient
 * @package VistaAPIClient
 ******************************************/

abstract class ApiClientAbstract
{
    abstract protected function getContent($service,$error_callback);

    /******************************************
     * A method that handles errors
     * @param $e
     * @param string $erroComplementLine1
     * @param string $erroComplementLine2
     * @param bool $logFinalLine
     * @return mixed
     ******************************************/

    public function StatusCodeHandling($e, $erroComplementLine1="", $erroComplementLine2="", $logFinalLine=false)
    {
        $response  = json_decode($e->getResponse()->getBody(true)->getContents());
        $cod_error = $e->getResponse()->getStatusCode();

        switch ($cod_error) {
            case "401":
                $error_msg = "Unauthorized";
                break;
            case "403":
                $error_msg = "Forbidden";
                break;
            case "404":
                $error_msg = "Not Found";
                break;
            case "405":
                $error_msg = "Method not allowed";
                break;
            case "410":
                $error_msg = "Gone";
            case "501":
                $error_msg = "Not Implemented";
                break;

            default:
                $error_msg = "Unknown error";
        }

        if (!empty($erroComplementLine1)) {
            LogConsole::printMessage("error", [ $erroComplementLine1 ], null, false);
        }
        if (!empty($erroComplementLine2)) {
            LogConsole::printMessage("error", [ $erroComplementLine2 ], null, false);
        }

        LogConsole::printMessage("error", [
            "Cod: ".$error_msg,
            $response->data->status." - ".$response->message ], false);

        if ($logFinalLine) {
            LogConsole::line("error");
        }

        return $response;
    }

}

