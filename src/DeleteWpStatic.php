<?php
/**
 * Created by PhpStorm.
 * User: lucio
 * Date: 12/09/17
 * Time: 10:57
 * Delete in Wordpress Register not fond in Vista
 */

namespace VistaIntegration;
use GuzzleHttp\Exception\RequestException;

/*******************************************
 * Class DeleteWpStatic
 * @package VistaIntegration
 *******************************************/

class DeleteWpStatic extends WpApiClient
{
    private $wp_sessions;
    private $wp_cinemas;
    private $wp_films;
    private $VistaActiveRegister;

    /*******************************************
     * @param $wp_sessions
     * @param StoreReferenceToExclude $VistaActiveRegister
     * @param WpApiClient $WpApiobject
     *******************************************/

    public static function ExcludeSessions($wp_sessions, StoreReferenceToExclude $VistaActiveRegister, WpApiClient $WpApiobject )
    {
        foreach ($wp_sessions as $session){
            if (self::filter_by_key($VistaActiveRegister->Vista_TotalActivesSessions, $session->sessionid) == null)
            {
                $url = self::API_URL . "/filmsession/$session->id/";

                try {
                    $response = self::deleteAction($url, $WpApiobject );

                    LogConsole::printMessage("success", [
                        "WP Session DELETED -> " . $url,
                        $response->getReasonPhrase() . " - Status: " . $response->getStatusCode(),
                        "Session ID: " . $session->id
                    ]);

                } catch (RequestException $e) {
                    $erroComplementLine1 = "WP DELETE Sessions ERROR -> " . $url;
                    $erroComplementLine2 = "Session ID: " . $session->id;
                    $WpApiobject->StatusCodeHandling($e, $erroComplementLine1, $erroComplementLine2);
                }
            }
        }
        unset($wp_sessions);
    }

    /*******************************************
     * @param $wp_films
     * @param StoreReferenceToExclude $VistaActiveRegister
     * @param WpApiClient $WpApiobject
     *******************************************/

    public static function ExcludeFilms($wp_films, StoreReferenceToExclude $VistaActiveRegister, WpApiClient $WpApiobject )
    {
        foreach ($wp_films as $film){
            if (self::filter_by_key($VistaActiveRegister->Vista_TotalActivesFilms, $film->filmhopk) == null)
            {
                $url = self::API_URL . "/film/$film->id/";

                try {
                    $response = self::deleteAction($url, $WpApiobject );

                    LogConsole::printMessage("success", [
                        "WP Film DELETED -> " . $url,
                        $response->getReasonPhrase() . " - Status: " . $response->getStatusCode(),
                        "Film ID: " . $film->id
                    ]);

                } catch (RequestException $e) {
                    $erroComplementLine1 = "WP DELETE Film ERROR -> " . $url;
                    $erroComplementLine2 = "Film ID: " . $film->id;
                    $WpApiobject->StatusCodeHandling($e, $erroComplementLine1, $erroComplementLine2);
                }
            }
        }
        unset($wp_films);
    }

    /*******************************************
     * @param $wp_films
     * @param StoreReferenceToExclude $VistaActiveRegister
     * @param WpApiClient $WpApiobject
     *******************************************/

    public static function ExcludeCinemas($wp_cinemas , StoreReferenceToExclude $VistaActiveRegister, WpApiClient $WpApiobject )
    {
        foreach ($wp_cinemas  as $cinema){
            if (self::filter_by_key($VistaActiveRegister->Vista_TotalActivesCinema, $cinema->cinema_vista_id) == null)
            {
                $url = self::API_URL . "/cinema/$cinema->id/";

                try {
                    $response = self::deleteAction($url, $WpApiobject );

                    LogConsole::printMessage("success", [
                        "WP Cinema DELETED -> " . $url,
                        $response->getReasonPhrase() . " - Status: " . $response->getStatusCode(),
                        "Cinema ID: " . $cinema->id
                    ]);

                } catch (RequestException $e) {
                    $erroComplementLine1 = "WP DELETE Cinema ERROR -> " . $url;
                    $erroComplementLine2 = "Cinema ID: " . $cinema->id;
                    $WpApiobject->StatusCodeHandling($e, $erroComplementLine1, $erroComplementLine2);
                }
            }
        }
        unset($wp_films);
    }

    /*******************************************
     * @param $array
     * @param $value
     * @return bool
     *******************************************/

    public static function filter_by_key($array, $value) {
        foreach($array as $v) {
            if($v == $value) {
                return true;
            }
        }
        return false;
    }

    /*******************************************
     * @param $url
     * @param WpApiClient $WpApiobject
     * @return \Psr\Http\Message\ResponseInterface
     *******************************************/

    private static function deleteAction($url, WpApiClient $WpApiobject )
    {

        /*
        $response = $WpApiobject->client->delete($url, [
            'headers' => ['Content-Type' => 'application/json',
                'Authorization' => 'Basic ' . base64_encode(self::USERNAME . ':' . self::PASSWORD)]
        ]);
        */

        $response = $WpApiobject->client->delete($url, [
            'headers' => ['Content-Type' => 'application/json']
        ]);

        return $response;
    }

}