<?php
/**
 * Created by PhpStorm.
 * User: lucio
 * Date: 05/09/17
 * Time: 11:23
 *
 * Class contains search utilities from API arrays objects
 *
 */

namespace VistaIntegration;

class FinderMemberUtilities
{

    public static function filter_by_key($array, $member, $value, &$retorn_id=null) {
        $filtered = array();
        foreach($array as $k => $v) {
            if($v->$member == $value) {
                $filtered[0] = $v;
                if ($retorn_id <> null) {
                    $retorn_id = $v->id;
                }
                break;
            }
        }
        return $filtered;
    }

}

class FinderMemberUtilities_2
{

    public static function filter_by_key($array, $member, $value, $cinema) {
        $filtered = array();
        foreach($array as $k => $v) {
            if (($v->$member == $value) and ($v->cinema_id_vista == $cinema)) {
                $filtered[0] = $v;
                break;
            }
        }
        return $filtered;
    }

}