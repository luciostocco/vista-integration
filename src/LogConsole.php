<?php
/**
 * Created by PhpStorm.
 * User: lucio
 * Date: 10/09/17
 * Time: 12:28
 */

use Phasty\Log\File as Logfile;

namespace VistaIntegration;

class LogConsole extends \Phasty\Log\File
{
    static public function configure()
    {
        self::$config['header']['format'] = '[^PID, %H:%i:%s]';
        self::$config['spacer'] = "+--";
        self::$config['path'] = "log/";
        self::$config['colors']['blue']   =  array( 'header' => "", 'message' => 'blue');
        self::$config['colors']['lgreen'] =  array( 'header' => "", 'message' => 'lgreen');
        self::$config['colors']['white']  =  array( 'header' => "", 'message' => 'white');
        self::$config['colors']['yellow'] =  array( 'header' => "", 'message' => 'yellow');

        // Log Settings
        self::config(require "config/log.php");
     }

    static public function lgreen($msg, $section = null) {
        static::messagexxx(__FUNCTION__, $msg, $section);
    }

    static public function blue($msg, $section = null) {
        static::messagexxx(__FUNCTION__, $msg, $section);
    }

    static public function yellow($msg, $section = null) {
        static::messagexxx(__FUNCTION__, $msg, $section);
    }

    static public function white($msg, $section = null) {
        static::messagexxx(__FUNCTION__, $msg, $section);
    }

    static protected function messagexxx($level, $msg, $section = null)
    {
        self::message($level, $msg, $section);
    }

   static public function printMessage($color="white", $messages, $section = null, $logFinalLine=true)
   {
       foreach ($messages as $message) {
           self::message($color, $message, $section);  // log
           echo $message. "\n";  // print on screem
       }
       if ($logFinalLine) {
           self::line($color);
           echo "------------------------------------------------------------------". "\n";
       }
   }

   static public function line($color) {
       self::message($color, "------------------------------------------------------------------", false);
       echo "------------------------------------------------------------------". "\n";
   }



}