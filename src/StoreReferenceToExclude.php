<?php
/**
 * Created by PhpStorm.
 * User: lucio
 * Date: 12/09/17
 * Time: 10:39
 */

namespace VistaIntegration;


class StoreReferenceToExclude
{
    public $Vista_TotalActivesCinema;
    public $Vista_TotalActivesSessions;
    public $Vista_TotalActivesFilms;

    public function __construct()
    {
        $VistaTotalActivesCinema   = array();
        $VistaTotalActivesSessions = array();
        $VistaTotalActivesFilms    = array();
    }

}