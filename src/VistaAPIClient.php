<?php
/**
 * Created by PhpStorm.
 * User: lucio
 * Date: 02/09/17
 * Time: 18:06
 */

namespace VistaIntegration;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;


/******************************************
 * Class VistaApiClient
 * @package VistaApiClient
 * Vista API Class
 ******************************************/

class VistaApiClient extends ApiClientAbstract
{
    private $client = null;
    const API_URL =  LoadConfig::VISTA_API_BASE_URL;
    var $accessToken;

    public function __construct($accessToken)
    {
        $this->$accessToken = $accessToken;
        $this->client   = new Client();
    }

    public function prepare_access_token() {
        // When developed need to finish function StatusCodeHandling($e)
        // if ($e->getResponse()->getStatusCode() == '400')
    }

    /******************************************
     * @param $service
     * @param string $error_callback
     * @return mixed|\Psr\Http\Message\ResponseInterface
     *******************************************/

    public function getContent($url,$error_callback="")
    {
        try {

            $header = array(
                "Content-Type"=>"application/json",
                'request.options' => array (
                    'timeout' => 6,
                    'connect_timeout' => 6
                )
            );

            putenv('RES_OPTIONS=retrans:1 retry:1 timeout:1 attempts:1'); //dns resolve params

            $response = $this->client->get($url, array("headers" => $header));
            $result = $response->getBody()->getContents();

            $json_a = json_decode($result);
            return $json_a;

        } catch (RequestException $e)
        {

            $response = $this->StatusCodeHandling($e);

            if (!empty($error_callback)) { $error_callback($response); }

            return $response;
        }
    }

    /******************************************
     *
     * @summary  Load all Cinemas from Vista
     * @return   Object film_session
     *
     ******************************************/

    public function loadCinemas()
    {
        $url = self::API_URL . '/Cinemas?$format=json' ;

        LogConsole::printMessage("notice", [
            "Loading Cinemas from Vista...",
            "URL: ". $url
        ]);

        $load = $this->getContent($url,( function($response)
        {
            LogConsole::printMessage("error", [
                "Error - API Cinemas get list",
                "Script canceled.. / Source code: SC06" ]);
            die();
        }  ));

        return $load;
    }


    public function loadFilmGenres()
    {
        $url = self::API_URL . '/FilmGenres?$format=json' ;

        LogConsole::printMessage("notice", [
            "Loading Film Genres from Vista...",
            "URL: ". $url
        ]);

        $load = $this->getContent($url,( function($response)
        {
            LogConsole::printMessage("error", [
                "Error - Film Genres get list" ]);
        }  ));

        return $load;
    }


    /******************************************
     *
     * @summary  Load all Cinemas from Vista
     * @return   Object film_session
     *
     ******************************************/

    public function loadFilmsExtensions($ScheduledFilmId)
    {
        $url = self::API_URL . '/ScheduledFilms?$filter=ScheduledFilmId%20eq%20%27' . $ScheduledFilmId . '%27&$format=json';

        LogConsole::printMessage("notice", [
            "Loading Cinemas imagem and trailer URL from Vista...",
            "URL: ". $url
        ]);

        $load = $this->getContent($url,( function($response)
        {
            LogConsole::printMessage("error", [
                "Error - API Cinemas get imagem and trailer URL from Vista",
                "Script canceled.. / Source code: SC45" ]);
            die();
        }  ));

        return $load;
    }



    public function searchFilm($id)
    {
        $url = self::API_URL . '/Films(\'' . $id . '\')?$format=json' ;

        LogConsole::printMessage("notice", [
            "Loading Films from Vista...",
            "URL: ". $url
        ]);

        $load = $this->getContent($url,( function($response)
        {
            LogConsole::printMessage("error", [
                "Error - Films get list" ]);
        }  ));

        return $load;
    }

    /*******************************************
     *
     * @summary Requesting a list of films that are currently showing at a particular Cinema
     * @param $cinema
     * @return mixed|\Psr\Http\Message\ResponseInterface
     *
     ******************************************/

    public function loadFilmsCurrentlyShowingByCinema($cinema)
    {
        $cinema_search = urlencode("'$cinema'");

        $url = self::API_URL . '/GetNowShowingScheduledFilms?$format=json&cinemaId=' . $cinema_search ;

        LogConsole::printMessage("notice", [
            "Loading Films currently showing in the cinema: ".$cinema,
            "URL: ". $url
        ], null, false);

        $load = $this->getContent($url,( function($response)
        {
            LogConsole::printMessage("error", [
                "Error - API Loading Films currently showing in the cinema",
                "Script canceled.. / Source code: SC07" ]);
            die();
        }  ));

        LogConsole::line("notice");

        return $load;
    }

    /*******************************************
     *
     * @summary Loading Sessions currently showing in a particular Cinema and Film
     * @param $cinema
     * @param $film
     * @return mixed|\Psr\Http\Message\ResponseInterface
     ******************************************/

    public function loadSessionsShowingByFilmAndCinema($cinema,$film)
    {
        $cinema_search = urlencode("'$cinema'");
        $film_search = urlencode("'$film'");

        $url = self::API_URL . '/Sessions?$format=json&$filter=CinemaId%20eq%20'. $cinema_search .
            '%20and%20ScheduledFilmId%20eq%20'.$film_search .
            '&$select=SessionId,ScheduledFilmId,Showtime,SeatsAvailable,IsAllocatedSeating,AllowChildAdmits,AllowTicketSales,PriceGroupCode,SessionBusinessDate,CinemaId' ;

        LogConsole::printMessage("notice", [
            "Loading Sessions currently showing in the cinema: " . $cinema . " and Film: " . $film ,
            "URL: ". $url
        ], null, false);

        $load = $this->getContent($url, ( function($response)
                {
                    LogConsole::printMessage("error", [
                        "Error - Loading Sessions currently showing in a particular Cinema and Film",
                        "Script canceled.. / Source code: SC08" ]);
                     die();
                }
            )
        );

        LogConsole::line("notice");

        return $load;
    }

    /******************************************
     *  @summary Method that request extended film detail
     *  @param   string $film
     *  @return  array  $film_details['director']
     *                  $film_details['cast']
     ******************************************/

    public function requestExtendedFilmDetails($film)
    {
        $film_search = urlencode("'$film'");
        $url = self::API_URL . '/FilmPersonLinks?$format=json&$filter=FilmId%20eq%20'.$film_search;

        LogConsole::printMessage("notice", [
            "Loading extended film detail: Film: " . $film,
            "URL: ". $url
        ], null, false);

        // get all extended film detail
        $vista_film_details = $this->getContent($url,( function($response)
        {
            LogConsole::printMessage("error", [
                "Vista - Request extended film detail list -> Error",
                $url ,
                "The script will continue.. / Source code: SC05" ]);
             return null;
        }  ));

        LogConsole::line("notice");

        $film_details['director'] = "";
        $film_details['cast']     = "";

        foreach ($vista_film_details->value as $persona ) {

            $persona_search = urlencode("('".$persona->PersonId."')");
            $url = self::API_URL . "/Persons".$persona_search.'?$format=json';

            LogConsole::printMessage("notice", [
                "Loading persona detail: Persona: " . $persona->PersonId,
                "URL: ". $url
            ], null, false);

            $erro_ = false;
            // Get persona details from Vista'
            $vista_persona_details = $this->getContent($url,( function($response)
            {
                LogConsole::printMessage("error", [
                    "Vista - Request persona film detail list -> Error",
                    $url ,
                    "PersonId: " . $persona->PersonId,
                    "The script will continue.. / Source code: SC06" ]);
                $erro_ = true;
            }  ));

            if (!$erro_) {
                if ($persona->PersonType == "D") {
                    if (empty($film_details['director'])) {
                        $film_details['director'] = $vista_persona_details->FirstName . " " . $vista_persona_details->LastName;
                    } else {
                        $film_details['director'] = $film_details['director'] . ", " . $vista_persona_details->FirstName . " " . $vista_persona_details->LastName;
                    }
                } else {
                    if (empty($film_details['cast'])) {
                        $film_details['cast'] = $vista_persona_details->FirstName . " " . $vista_persona_details->LastName;
                    } else {
                        $film_details['cast'] = $film_details['cast'] . ", " . $vista_persona_details->FirstName . " " . $vista_persona_details->LastName;
                    }
                }
            }

            LogConsole::line("notice");
        }

        return $film_details;
    }


}









