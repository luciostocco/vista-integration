<?php

/******************************************
 *
 * Class WpAPIClient
 * @package VistaAPIClient
 * Integration Wordpress API Class
 *
 ******************************************/

namespace VistaIntegration;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use VistaIntegration\VistaApiClient;
use VistaIntegration\LoadConfig;


class WpApiClient extends ApiClientAbstract
{
    public $client = null;
    private $vista_api;

    const API_URL  = LoadConfig::WORDPRESS_API_BASE_URL;
    const USERNAME = LoadConfig::WORDPRESS_API_USERNAME;
    const PASSWORD = LoadConfig::WORDPRESS_API_PASSWORD;

    var $accessToken;

    public function __construct()
    {
        //$this->$accessToken = $accessToken;

        $this->client = new Client(['allow_redirects' => ['track_redirects' => true]]);
        $this->vista_api = new VistaApiClient(LoadConfig::VISTA_ACCESS_TOKEN);

        return;
    }

    public function prepare_access_token()
    {
        // When developed need to finish function StatusCodeHandling($e)
        // if ($e->getResponse()->getStatusCode() == '400')
    }


    /******************************************
     *  Request all content from $service
     *  Used for all services
     ******************************************/

    public function getContent($url, $error_callback)
    {
        try {

            $header = array(
                "Content-Type" => "application/json",
                'request.options' => array (
                    'timeout' => 6,
                    'connect_timeout' => 6
                )
            );

            putenv('RES_OPTIONS=retrans:1 retry:1 timeout:1 attempts:1'); //dns resolve params

            $response = $this->client->get($url, array("headers" => $header));
            $result_page = $response->getBody()->getContents();

            $result = array();
            $result[] = json_decode($result_page);

            $res = $result[0]->objects;

            return $res;

        } catch (RequestException $e) {

            $response = $this->StatusCodeHandling($e);

            $error_callback($response);
            return $response;

        }
    }

    /******************************************
     *
     * @param $url
     * @param $body
     * @param $executa_json_encode
     * @return \Psr\Http\Message\ResponseInterface
     *
     ******************************************/

    private function postAction($url, $body, $executa_json_encode = true)
    {
        if ($executa_json_encode) {
            $body_json = json_encode($body);
        } else {
            $body_json = $body;
        }

        /*
        $response = $this->client->post($url, [
            'headers' => ['Content-Type' => 'application/json',
                'Authorization' => 'Basic ' . base64_encode(self::USERNAME . ':' . self::PASSWORD)],
            'body' => $body_json
        ]);
        */

        $response = $this->client->post($url, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body_json
        ]);

        return $response;
    }

    /******************************************
     *
     * @param $url
     * @param $body
     * @param $executa_json_encode
     * @return \Psr\Http\Message\ResponseInterface
     *
     ******************************************/

    private function putAction($url, $body, $executa_json_encode = true)
    {
        if ($executa_json_encode) {
            $body_json = json_encode($body);
        } else {
            $body_json = $body;
        }

        /*
        $response = $this->client->post($url, [
            'headers' => ['Content-Type' => 'application/json',
                'Authorization' => 'Basic ' . base64_encode(self::USERNAME . ':' . self::PASSWORD)],
            'body' => $body_json
        ]);
        */

        $response = $this->client->put($url, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body_json
        ]);

        return $response;
    }


    /******************************************
     *
     * @summary  Load all Sessions from wordpress
     * @return   Object film_session
     *
     ******************************************/

    public function loadSessions()
    {
        // $url = self::API_URL . "/film_session?per_page=100";
        $url = self::API_URL . "/filmsession";


        LogConsole::printMessage("notice", [
            "Loading Sessions from wordpress...",
            "URL: " . $url
        ], null, false);


        $load = $this->getContent($url, (function ($response) {
            LogConsole::printMessage("error", [
                "Wordpress film_essions get list -> Error",
                "Script canceled.. / Source code: SC01"
            ]);
            die();
        }));

        LogConsole::line("notice");

        return $load;
    }

    /******************************************
     *
     * @summary  Load Cinemas from wordpress
     * @return   Object cinema
     *
     ******************************************/

    public function loadCinemas()
    {
       // $url = self::API_URL . "/cinema?per_page=100";

        $url = self::API_URL . "/cinema";

        LogConsole::printMessage("notice", [
            "Loading Cinemas from wordpress...",
            "URL: " . $url
        ], null, false);

        $load = $this->getContent($url, (function ($response) {
            LogConsole::printMessage("error", [
                "Wordpress Cinemas get list -> Error",
                "Script canceled.. / Source code: SC02"
            ]);
            die();
        }));

        LogConsole::line("notice");

        return $load;
    }

    /******************************************
     *
     * @summary  Load Films from wordpress
     * @return   Object cinema
     *
     ******************************************/

    public function loadFilms()
    {
       // $url = self::API_URL . "/films?per_page=100";

        $url = self::API_URL . "/film";


        LogConsole::printMessage("notice", [
            "Loading Films from wordpress...",
            "URL: " . $url
        ], null, false);

        $load = $this->getContent($url, (function ($response) {
            LogConsole::printMessage("error", [
                "Wordpress Films load list -> Error",
                "Script canceled.. / Source code: SC13"
            ]);
           die();
        }));

        LogConsole::line("notice");

        return $load;
    }

    /******************************************
     * Insert or Updating Cinema in Wordpress from Vista->$cinema
     * Insert   => $service = "/cinema"
     * Updating => $service = "/cinema/id"
     ******************************************/

    public function insertUpdateCinema($service, $cinema, &$cinema_id_created, $insert)
    {

        try {
            $url = self::API_URL . $service;

            $body = [
                'cinema_vista_id' => $cinema->ID,
                'name' => $cinema->Name,
                'phonenumber' => $cinema->PhoneNumber,
                'address1' => $cinema->Address1,
                'address2' => $cinema->Address2,
                'city' => $cinema->City,
                'latitude' => $cinema->Latitude,
                'longitude' => $cinema->Longitude,
                'parkinginfo' => $cinema->ParkingInfo,
                'publictransport' => $cinema->PublicTransport
            ];

            // Only if is creating new Session
            if ($insert) {

                $response = $this->postAction($url, $body);

                // Get the Film Id after being created
                $cinema_id_created = $this->getCinemaIdAJustCreated($response);

            } else {   // Update

                $response = $this->putAction($url, $body);

            }

            LogConsole::printMessage("success", [
                "Cinema POST -> " . $url,
                $response->getReasonPhrase() . " - Status: " . $response->getStatusCode(),
                "Cinema ID: " . $cinema->ID . " - " . $cinema->Name . " / " . $cinema_id_created
            ]);

            return true;

        } catch (RequestException $e) {
            $erroComplementLine1 = "Cinema POST ERROR -> " . $url;
            $erroComplementLine2 = "Cinema ID: " . $cinema->ID . " - " . $cinema->Name;
            $response = $this->StatusCodeHandling($e, $erroComplementLine1, $erroComplementLine2);

            return false;
        }
    }


    /******************************************
     *
     * Insert or Updating Sessions in Wordpress from Vista
     * Insert   => $service = "/cinema"
     * Updating => $service = "/cinema/id"
     *
     ******************************************/

    public function insertUpdateSession($service, $session, $cinema, $film, &$film_id_created, $insert, $cinema_id_wp, $film_title)
    {

        try {
            $url = self::API_URL . $service;


            $infos_from_title = $this->get_info_from_title($film_title);

            // $_title    = (isset($infos_from_title[3]) ? $infos_from_title[3] : $film->Title);
            $_format   = (isset($infos_from_title[0]) ? $infos_from_title[0] : '');
            $_language = (isset($infos_from_title[1]) ? $infos_from_title[1] : '');
            $_subtitle = (isset($infos_from_title[2]) ? $infos_from_title[2] : '');


            $body = [
                'sessionid' => $session->SessionId,
                'description' => "Film Vista: " . $film . " Cod. Cinema Vista: " . $cinema . " Cod. Cinema WP: " . $cinema_id_wp,
                'cinema' => "/api/cinema/" . $cinema_id_wp . "/",
                'film' => "/api/film/" . $film_id_created . "/",
                'session_date' => $new_date_format = date('Y-m-d', strtotime($session->Showtime)),
                'session_time' => $new_date_format = date('H:i', strtotime($session->Showtime)),
                'session_language' => $_language,
                'session_subtitle' => $_subtitle,
                'session_format'   => $_format,
                'showtime' => $session->Showtime,
                'seatsavailable' => $session->SeatsAvailable,
                'isallocatedseating' => $session->IsAllocatedSeating,
                'allowchildadmits' => $session->AllowChildAdmits,
                'allowticketsales' => $session->AllowTicketSales,
                'pricegroupcode' => $session->PriceGroupCode,
                'sessionbusinessdate' => $session->SessionBusinessDate,
                'scheduledFilmId' => $session->ScheduledFilmId,
                'cinema_id_vista' => $session->CinemaId,
            ];

            // Only if is creating new Session
            if ($insert) {

                $response = $this->postAction($url, $body);

                // Get the Film Id after being created
              //  $film_id_created = $this->getSessionIdAJustCreated($response);

            } else {
                $response = $this->putAction($url, $body);

            }

            LogConsole::printMessage("white", [
                "Session POST -> " . $url,
                $response->getReasonPhrase() . " - Status: " . $response->getStatusCode(),
                "Session ID: " . $session->SessionId . " - Cinema Vista : " . $cinema . " - Cinema WP : " . $cinema_id_wp . " - Film: " . $film
            ]);

            return true;

        } catch (RequestException $e) {
            $erroComplementLine1 = "SESSION ERROR -> " . $url;
            $erroComplementLine2 = "Session ID: " . $session->SessionId . " - Cinema Vista : " . $cinema . " - Cinema WP : " . $cinema_id_wp . " - Film: " . $film;
            $response = $this->StatusCodeHandling($e, $erroComplementLine1, $erroComplementLine2);

            return false;
        }
    }

    /****************************************
     * @summary  Get the Session Id after being created
     * @param $response
     * @return mixed
     ****************************************/

    public function getSessionIdAJustCreated($response)
    {
        $parce1 = $response->getHeaders()['Location'][0];
        $parce2 = parse_url($parce1)['path'];
        preg_match("/film_session\/(\d+)/", $parce2, $match);
        $ret = $match[1];
        return $ret;
    }

    /****************************************
     * @summary  Get the Cinema Id after being created
     * @param $response
     * @return mixed
     ****************************************/

    public function getCinemaIdAJustCreated($response)
    {
        $parce1 = $response->getHeaders()['Location'][0];
        $parce2 = parse_url($parce1)['path'];
        preg_match("/cinema\/(\d+)/", $parce2, $match);
        $ret = $match[1];
        return $ret;
    }

    /****************************************
     * @summary  Get the Session Id after being created
     * @param $response
     * @return mixed
     ****************************************/

    public function getFilmIdAJustCreated($response)
    {
        $parce1 = $response->getHeaders()['Location'][0];
        $parce2 = parse_url($parce1)['path'];
        preg_match("/film\/(\d+)/", $parce2, $match);
        $ret = $match[1];
        return $ret;
    }

    /****************************************
     *
     * @summary  Create/update Sessions for a particular film and cinema
     * @param $wp_sessions
     * @param $session
     * @param $cinema
     * @param $film
     * @param $session_array (by reference)
     *
     ****************************************/

    public function sessionsOperations($wp_sessions, $session, $cinema, $film, &$session_array, $cinema_id_wp, $film_id_created)
    {
        // Search if exist vista.sessions->SessionId in wordpress.film_session.SessionId
        $array = FinderMemberUtilities::filter_by_key($wp_sessions, 'sessionid', $session->SessionId);

        // 5) Create or updating sessions in Wordpress
        if (empty($array)) {
            // Insert new Session in Wordpress
            $success = $this->insertUpdateSession("/filmsession/", $session, $cinema->ID, $film->FilmHOPK, $film_id_created, true, $cinema_id_wp, $film->Title);

            if ($success) {
                $session_array[] = $film_id_created;
            } else {
                LogConsole::printMessage("error", [
                    "Error creating Session in Wordpress -> film_session",
                    "The script will continue.. / Source code: SC11", ]);
             }

            $msg = "creating";
        } else {
            // Update Session Wordpress
            $success = $this->insertUpdateSession("/filmsession/" . $array[0]->id . "/", $session, $cinema->ID . "/", $film->FilmHOPK, $film_id_created, false, $cinema_id_wp, $film->Title);

            if ($success) {
                $session_array[] = $array[0]->id;
            } else {
                LogConsole::printMessage("error", [
                    "Error updating Session in Wordpress -> film_session",
                    "The script will continue.. / Source code: SC12", ]);
            }
        }
        return;
    }

    /****************************************
     *
     * @summary  Create or updating cinema in Wordpress
     * @param $wp_cinemas
     * @param $cinema
     *
     ****************************************/

    public function cinemaOperations($wp_cinemas, $cinema, &$cinema_id_created)
    {
        $cinema_id_created_2_level = $cinema_id_created;

        // Search if exist vista.cinema->ID in wordpress.cinema.cinema_vista_id
        $array = FinderMemberUtilities::filter_by_key($wp_cinemas, 'cinema_vista_id', $cinema->ID, $cinema_id_created_2_level);

        // 2) Create or updating cinema in Wordpress
        if (empty($array)) {
            // Insert new Cinema in Wordpress
            $this->insertUpdateCinema("/cinema/", $cinema, $cinema_id_created_2_level, true);
        } else {
            // Update Cinemain Wordpress
            $this->insertUpdateCinema("/cinema/" . $array[0]->id . "/", $cinema, $cinema_id_created_2_level , false);
        }

        $cinema_id_created = $cinema_id_created_2_level;

        return;
    }

    /****************************************
     * @param $wp_sessions
     * @param $session
     * @param $cinema
     * @param $film
     * @param $session_array
     ****************************************/

    public function filmOperations($wp_films, $film, $session_array, $extended_film_details, $cinema, $cinema_id_created, $vista_film_genres, $vista_api, &$film_id_created )
    {
        // Search if exist vista.sessions->SessionId in wordpress.film_session.SessionId
        $array = FinderMemberUtilities_2::filter_by_key($wp_films, 'filmhopk', $film->FilmHOPK, $cinema);

        // Search film to get GenreID
        $film_search = $vista_api->searchFilm($film->FilmHOPK);

        // Search film_genre
        $film_genre = FinderMemberUtilities::filter_by_key($vista_film_genres->value, 'ID', $film_search->GenreId);


        // 5) Create or updating sessions in Wordpress
        if (empty($array)) {

            $insert = true;

            $id_created = 0;

            // Insert new Session in Wordpress
            $success = $this->insertUpdateFilm("/film/", $film, $film_search, $session_array, $extended_film_details, $cinema, $film_genre, $cinema_id_created, $insert, $id_created);

            $film_id_created = $id_created;

            if (!$success) {
                LogConsole::printMessage("error", [
                    "Error creating Session in Wordpress -> film_session",
                    "The script will continue.. / Source code: SC10" ]);
            }

        } else {

            $insert = false;
            $film_id_created = $array[0]->id;  // put the id that is being updating

            // Update Session Wordpress
            $success = $this->insertUpdateFilm("/film/" . $array[0]->id . "/", $film , $film_search, $session_array, $extended_film_details, $cinema, $film_genre, $cinema_id_created, $insert, $id_created);

            if (!$success) {
                LogConsole::printMessage("error", [
                    "Error updating Film in Wordpress",
                    "The script will continue.. / Source code: SC09" ]);
           }
        }
        return;
    }

    /**************************************
     * @param $service
     * @param $film
     * @param $session_array
     * @param $extended_film_details
     * @param $cinema
     * @return bool
     **************************************/

    public function insertUpdateFilm($service, $film, $film_search, $session_array, $extended_film_details, $cinema, $film_genre, $cinema_id_created, $insert, &$id_created)
    {

        try {
            $url = self::API_URL . $service;

            $_film_directed_by = $extended_film_details['director'];
            $_film_cast = $extended_film_details['cast'];
            $_film_release_date = $new_date_format = date('Y-m-d', strtotime($film->OpeningDate));
            $_film_sessions = "[ " . implode(', ', $session_array) . " ]";

            $_film_synopsis_details = preg_replace('/[^A-Za-z0-9\. -]/', '', $film->Synopsis);

            //$infos_from_title = $this->get_info_from_title($film->Title);

            //$_title = (isset($infos_from_title[3]) ? $infos_from_title[3] : $film->Title);

            $_title = $film->Title;    //preg_replace('/[^A-Za-z0-9\. -]/', '', $film->Title);
            $_genre = $film_genre[0]->Name;


            $infos_from_title = $this->get_info_from_title($film->Title);

            // $_title    = (isset($infos_from_title[3]) ? $infos_from_title[3] : $film->Title);
            $_format   = (isset($infos_from_title[0]) ? $infos_from_title[0] : '');

            $film_extension = $this->vista_api->loadFilmsExtensions($film->FilmHOPK);
            
            $trailer =  (isset($film_extension->value[0]->TrailerUrl)) ? $film_extension->value[0]->TrailerUrl : '';
            $image   =  (isset($film_extension->value[0]->GraphicUrl)) ? $film_extension->value[0]->GraphicUrl : '';

            $body = [
                'filmhopk' => $film->FilmHOPK,
                'cinema_id_vista' => $cinema,
                'name' => $_title . " - Cinema: " . $cinema,
                'filme_title' => $_title,
                'film_synopsis_details' => $_film_synopsis_details,
                'film_genre' => $_genre,
                'film_rating' => $film->Rating,
                'film_cover_image' => $image,
                'film_duration' => $film->RunTime,
                'film_directed_by' => $_film_directed_by,
                'film_cast' => $_film_cast,
                'film_release_date' => $_film_release_date,
                'film_trailler' => $trailer,
                'film_audience' => "",
                'cinema' => "/api/cinema/" . $cinema_id_created . "/",
                'film_format' => $_format,
            ];



            if ($insert) {
                $response = $this->postAction($url, $body, true);

                // Get the Film Id after being created
                $id_created = $this->getFilmIdAJustCreated($response);


            } else {
                $response = $this->putAction($url, $body, true);

            }

            LogConsole::printMessage("info", [
                "Film POST -> " . $url,
                $response->getReasonPhrase() . " - Status: " . $response->getStatusCode(),
                "Film: " . $film->FilmHOPK . " - " . substr($_title, 0, 10) . " - Cinema: " . $cinema . " Sessions: " . $_film_sessions
            ]);

            return true;

        } catch (RequestException $e) {
            $erroComplementLine1 = "FILM ERROR -> " . $url;
            $erroComplementLine2 = "Film: " . $film->FilmHOPK . "Cinema: " . $cinema;
            $response = $this->StatusCodeHandling($e, $erroComplementLine1, $erroComplementLine2);

            return false;
        }

    }

    /**
     * @param $string
     * @param string $start
     * @param string $end
     * @return array|string
     */
     public function get_info_from_title($string, $start='(', $end =')'){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        $extract = substr($string, $ini, $len);

        $clean_texto = str_replace($extract,"",$string);
        $clean_texto = str_replace('(', '', $clean_texto);
        $clean_texto = str_replace(')', '', $clean_texto);

        $pieces = explode(",", $extract);

        if (!empty($pieces)) {
            $pieces[3] = $clean_texto;
        }

        return $pieces;
     }
}
